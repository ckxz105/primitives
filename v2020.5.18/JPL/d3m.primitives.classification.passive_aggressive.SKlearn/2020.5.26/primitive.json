{
  "algorithm_types": [
    "PASSIVE_AGGRESSIVE"
  ],
  "name": "sklearn.linear_model.passive_aggressive.PassiveAggressiveClassifier",
  "primitive_family": "CLASSIFICATION",
  "python_path": "d3m.primitives.classification.passive_aggressive.SKlearn",
  "source": {
    "name": "JPL",
    "contact": "mailto:shah@jpl.nasa.gov",
    "uris": [
      "https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues",
      "https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.PassiveAggressiveClassifier.html"
    ]
  },
  "version": "2020.5.26",
  "id": "85e5c88d-9eec-3452-8f2f-414f17d3e4d5",
  "hyperparams_to_tune": [
    "C"
  ],
  "installation": [
    {
      "type": "PIP",
      "package_uri": "git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@1c12b1b9e6d138a1936389a892b02a98db9f4691#egg=sklearn_wrap"
    }
  ],
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
  "original_python_path": "sklearn_wrap.SKPassiveAggressiveClassifier.SKPassiveAggressiveClassifier",
  "primitive_code": {
    "class_type_arguments": {
      "Inputs": "d3m.container.pandas.DataFrame",
      "Outputs": "d3m.container.pandas.DataFrame",
      "Params": "sklearn_wrap.SKPassiveAggressiveClassifier.Params",
      "Hyperparams": "sklearn_wrap.SKPassiveAggressiveClassifier.Hyperparams"
    },
    "interfaces_version": "2020.5.18",
    "interfaces": [
      "supervised_learning.SupervisedLearnerPrimitiveBase",
      "base.PrimitiveBase",
      "base.ContinueFitMixin"
    ],
    "hyperparams": {
      "C": {
        "type": "d3m.metadata.hyperparams.Bounded",
        "default": 1,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "lower": 0,
        "upper": null,
        "lower_inclusive": true,
        "upper_inclusive": false
      },
      "fit_intercept": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Whether the intercept should be estimated or not. If False, the data is assumed to be already centered."
      },
      "max_iter": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": 1000,
        "structural_type": "typing.Union[NoneType, int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "configuration": {
          "int": {
            "type": "d3m.metadata.hyperparams.Bounded",
            "default": 1000,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ],
            "lower": 0,
            "upper": null,
            "lower_inclusive": true,
            "upper_inclusive": false
          },
          "none": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": null,
            "structural_type": "NoneType",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "shuffle": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Whether or not the training data should be shuffled after each epoch."
      },
      "tol": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": 0.001,
        "structural_type": "typing.Union[NoneType, float]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "configuration": {
          "float": {
            "type": "d3m.metadata.hyperparams.Bounded",
            "default": 0.001,
            "structural_type": "float",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ],
            "lower": 0,
            "upper": null,
            "lower_inclusive": true,
            "upper_inclusive": false
          },
          "none": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": null,
            "structural_type": "NoneType",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "n_jobs": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": 1,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter",
          "https://metadata.datadrivendiscovery.org/types/CPUResourcesUseParameter"
        ],
        "description": "The number of CPUs to use to do the OVA (One Versus All, for multi-class problems) computation. -1 means 'all CPUs'. Defaults to 1.",
        "configuration": {
          "limit": {
            "type": "d3m.metadata.hyperparams.Bounded",
            "default": 1,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ],
            "lower": 1,
            "upper": null,
            "lower_inclusive": true,
            "upper_inclusive": false
          },
          "all_cores": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": -1,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "loss": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "hinge",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "The loss function to be used: hinge: equivalent to PA-I in the reference paper. squared_hinge: equivalent to PA-II in the reference paper.",
        "values": [
          "hinge",
          "squared_hinge"
        ]
      },
      "warm_start": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution."
      },
      "class_weight": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": null,
        "structural_type": "typing.Union[NoneType, str]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Preset for the class_weight fit parameter.  Weights associated with classes. If not given, all classes are supposed to have weight one.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``  .. versionadded:: 0.17 parameter *class_weight* to automatically weight samples.",
        "configuration": {
          "str": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": "balanced",
            "structural_type": "str",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          },
          "none": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": null,
            "structural_type": "NoneType",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "average": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": false,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "When set to True, computes the averaged SGD weights and stores the result in the coef_ attribute. If set to an int greater than 1, averaging will begin once the total number of samples seen reaches average. So average=10 will begin averaging after seeing 10 samples. New in version 0.19: parameter average to use weights averaging in SGD",
        "configuration": {
          "int": {
            "type": "d3m.metadata.hyperparams.Bounded",
            "default": 10,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ],
            "lower": 2,
            "upper": null,
            "lower_inclusive": true,
            "upper_inclusive": false
          },
          "bool": {
            "type": "d3m.metadata.hyperparams.UniformBool",
            "default": false,
            "structural_type": "bool",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "early_stopping": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Whether to use early stopping to terminate training when validation score is not improving. If set to True, it will automatically set asid a fraction of training data as validation and terminate training whe validation score is not improving by at least tol fo n_iter_no_change consecutive epochs."
      },
      "validation_fraction": {
        "type": "d3m.metadata.hyperparams.Bounded",
        "default": 0.1,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if early_stopping is True.",
        "lower": 0,
        "upper": 1,
        "lower_inclusive": true,
        "upper_inclusive": true
      },
      "n_iter_no_change": {
        "type": "d3m.metadata.hyperparams.Bounded",
        "default": 5,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Number of iterations with no improvement to wait before early stopping.",
        "lower": 0,
        "upper": null,
        "lower_inclusive": true,
        "upper_inclusive": false
      },
      "use_inputs_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "use_outputs_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "exclude_inputs_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "exclude_outputs_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "return_result": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "new",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
        "values": [
          "append",
          "replace",
          "new"
        ]
      },
      "use_semantic_types": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
      },
      "add_index_columns": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
      },
      "error_on_no_input": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False."
      },
      "return_semantic_type": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "https://metadata.datadrivendiscovery.org/types/PredictedTarget",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Decides what semantic type to attach to generated output",
        "values": [
          "https://metadata.datadrivendiscovery.org/types/Attribute",
          "https://metadata.datadrivendiscovery.org/types/ConstructedAttribute",
          "https://metadata.datadrivendiscovery.org/types/PredictedTarget"
        ]
      }
    },
    "arguments": {
      "hyperparams": {
        "type": "sklearn_wrap.SKPassiveAggressiveClassifier.Hyperparams",
        "kind": "RUNTIME"
      },
      "random_seed": {
        "type": "int",
        "kind": "RUNTIME",
        "default": 0
      },
      "docker_containers": {
        "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
        "kind": "RUNTIME",
        "default": null
      },
      "timeout": {
        "type": "typing.Union[NoneType, float]",
        "kind": "RUNTIME",
        "default": null
      },
      "iterations": {
        "type": "typing.Union[NoneType, int]",
        "kind": "RUNTIME",
        "default": null
      },
      "produce_methods": {
        "type": "typing.Sequence[str]",
        "kind": "RUNTIME"
      },
      "inputs": {
        "type": "d3m.container.pandas.DataFrame",
        "kind": "PIPELINE"
      },
      "outputs": {
        "type": "d3m.container.pandas.DataFrame",
        "kind": "PIPELINE"
      },
      "params": {
        "type": "sklearn_wrap.SKPassiveAggressiveClassifier.Params",
        "kind": "RUNTIME"
      }
    },
    "class_methods": {},
    "instance_methods": {
      "__init__": {
        "kind": "OTHER",
        "arguments": [
          "hyperparams",
          "random_seed",
          "docker_containers"
        ],
        "returns": "NoneType"
      },
      "continue_fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "Similar to base ``fit``, this method fits the primitive using inputs and outputs (if any)\nusing currently set training data.\n\nThe difference is what happens when currently set training data is different from\nwhat the primitive might have already been fitted on. ``fit`` resets parameters and\nrefits the primitive (restarts fitting), while ``continue_fit`` fits the primitive\nfurther on new training data. ``fit`` does **not** have to be called before ``continue_fit``,\ncalling ``continue_fit`` first starts fitting as well.\n\nCaller can still call ``continue_fit`` multiple times on the same training data as well,\nin which case primitive should try to improve the fit in the same way as with ``fit``.\n\nFrom the perspective of a caller of all other methods, the training data in effect\nis still just currently set training data. If a caller wants to call ``gradient_output``\non all data on which the primitive has been fitted through multiple calls of ``continue_fit``\non different training data, the caller should pass all this data themselves through\nanother call to ``set_training_data``, do not call ``fit`` or ``continue_fit`` again,\nand use ``gradient_output`` method. In this way primitives which truly support\ncontinuation of fitting and need only the latest data to do another fitting, do not\nhave to keep all past training data around themselves.\n\nIf a primitive supports this mixin, then both ``fit`` and ``continue_fit`` can be\ncalled. ``continue_fit`` always continues fitting, if it was started through ``fit``\nor ``continue_fit`` and fitting has not already finished. Calling ``fit`` always restarts\nfitting after ``continue_fit`` has been called, even if training data has not changed.\n\nPrimitives supporting this mixin and which operate on categorical target columns should\nuse ``all_distinct_values`` metadata to obtain which all values (labels) can be in\na target column, even if currently set training data does not contain all those values.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
      },
      "fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to fully refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
      },
      "fit_multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "outputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs:\n    The outputs given to ``set_training_data``.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "get_params": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "sklearn_wrap.SKPassiveAggressiveClassifier.Params",
        "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
      },
      "multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "produce": {
        "kind": "PRODUCE",
        "arguments": [
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
        "singleton": false,
        "inputs_across_samples": [],
        "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
      },
      "set_params": {
        "kind": "OTHER",
        "arguments": [
          "params"
        ],
        "returns": "NoneType",
        "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
      },
      "set_training_data": {
        "kind": "OTHER",
        "arguments": [
          "inputs",
          "outputs"
        ],
        "returns": "NoneType",
        "description": "Sets current training data of this primitive.\n\nThis marks training data as changed even if new training data is the same as\nprevious training data.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs:\n    The inputs.\noutputs:\n    The outputs."
      }
    },
    "class_attributes": {
      "logger": "logging.Logger",
      "metadata": "d3m.metadata.base.PrimitiveMetadata"
    },
    "instance_attributes": {
      "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
      "random_seed": "int",
      "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
      "volumes": "typing.Dict[str, str]",
      "temporary_directory": "typing.Union[NoneType, str]"
    },
    "params": {
      "coef_": "typing.Union[NoneType, numpy.ndarray]",
      "intercept_": "typing.Union[NoneType, numpy.ndarray]",
      "classes_": "typing.Union[NoneType, numpy.ndarray]",
      "_expanded_class_weight": "typing.Union[NoneType, numpy.ndarray]",
      "alpha": "typing.Union[NoneType, float]",
      "epsilon": "typing.Union[NoneType, float]",
      "eta0": "typing.Union[NoneType, float]",
      "l1_ratio": "typing.Union[NoneType, float]",
      "learning_rate": "typing.Union[NoneType, str]",
      "loss_function_": "object",
      "n_iter_": "typing.Union[NoneType, int]",
      "penalty": "typing.Union[NoneType, str]",
      "power_t": "typing.Union[NoneType, float]",
      "t_": "typing.Union[NoneType, float]",
      "average_coef_": "typing.Union[NoneType, numpy.ndarray]",
      "average_intercept_": "typing.Union[NoneType, numpy.ndarray]",
      "standard_coef_": "typing.Union[NoneType, numpy.ndarray]",
      "standard_intercept_": "typing.Union[NoneType, numpy.ndarray]",
      "input_column_names": "typing.Union[NoneType, typing.Any]",
      "target_names_": "typing.Union[NoneType, typing.Sequence[typing.Any]]",
      "training_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_column_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_columns_metadata_": "typing.Union[NoneType, typing.List[collections.OrderedDict]]"
    }
  },
  "structural_type": "sklearn_wrap.SKPassiveAggressiveClassifier.SKPassiveAggressiveClassifier",
  "description": "Primitive wrapping for sklearn PassiveAggressiveClassifier\n`sklearn documentation <https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.PassiveAggressiveClassifier.html>`_\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
  "digest": "c3ca4b02dbb811d1984c495a6a877c88c0c8ff84ca681fbe1fcdc30e70e97d53"
}